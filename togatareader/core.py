
import json
from xdg import BaseDirectory
from feedparser import parse 
from hashlib import sha256
from os import remove

# https://pyxdg.readthedocs.io/
# https://feedparser.readthedocs.io/

FEED_SAVE_PATH = BaseDirectory.save_data_path('togatareader/') + 'feeds.json'

def load_config():
    pass

def save_config():
    pass

def load_feeds():
    """
    Input: none
    Output: if FEED_SAVE_PATH exists as valid json,
    it will be returned loaded as a dictionary. Otherwise, an 
    empty dictionary.
    """
    try:
        with open(FEED_SAVE_PATH) as file:
            return json.load(file)
    except (FileNotFoundError, json.decoder.JSONDecodeError):
        return {}

# Idea: add bozo detection and warn whether to continue
def save_feed(feedlink):
    """
    Input: string, a link to an RSS or Atom feed.
    The function will parse the feed and save information about
    the feed itself to a json file at FEED_SAVE_PATH.
    """
    f = parse(feedlink)
    # encodes feedlink to sha256 then truncates after 16 characters
    feedcode = sha256((feedlink).encode()).hexdigest()[:16]
    # It's important to ensure that the feedlist is updated when
    # calling this function, so it's not passed as an argument.
    feedlist = load_feeds()

    d = {}
    d['feedlink'] = feedlink
    attributes = ('title', 'link', 'subtitle', 
                  'description', 'published_parsed')
    for attr in attributes:
        d[attr] = f.feed.get(attr, '')
    feedlist[feedcode] = d
    with open(FEED_SAVE_PATH, 'w') as file:
        json.dump(feedlist, file)

def delete_feed(feedcode):
    """
    Input: string, the program-assigned feedcode.
    The function will delete the corresponding entry in
    FEED_SAVE_PATH.
    Output: boolean, True if the deletion is carried out
    and False if it fails due to a KeyError.
    """
    # It's important to ensure that the feedlist is updated when
    # calling this function, so it's not passed as an argument.
    feedlist = load_feeds()
    with open(FEED_SAVE_PATH, 'w') as file:
        try:
            del feedlist[feedcode]
            json.dump(feedlist, file)
            return True
        except KeyError:
            return False

def load_archive(feedcode):
    """
    Input: string, the program-assigned feedcode.
    Output: if the corresponding archive file exists as valid json,
    it will be returned loaded as a dictionary. Otherwise, an 
    empty dictionary.
    """
    path = BaseDirectory.save_data_path('togatareader/archive/') + (feedcode
                                                                  + '.json')
    try:
        with open(path) as file:
            return json.load(file)
    except (FileNotFoundError, json.decoder.JSONDecodeError):
        return {}

def save_archive(feedcode): 
    """
    Input: string, the program-assigned feedcode.
    The function will parse the linked feed and save it to 
    a json file in $XDG_DATA_HOME/togatareader/archive/.
    """
    feedlink = load_feeds()[feedcode]['feedlink'] 
    archive = load_archive(feedcode)
    path = BaseDirectory.save_data_path('togatareader/archive/') + (feedcode
                                                                  + '.json')
    entries = parse(feedlink).entries

    for e in entries:
        d = {}
        attributes = ('title', 'link', 'id', 'published_parsed', 'description',
                      'updated_parsed', 'summary', 'content')
        for attr in attributes:
            d[attr] = getattr(e, attr, '')
        if d['id'] != '':
            # encodes entry id to sha256 then truncates after 16 characters
            entrycode = sha256((d['id']).encode()).hexdigest()[:16]
        else:
            # I can't figure out anything better than entry ID to use
            # as a permanent identifier, so if it's absent,
            # it'll throw an error.
            raise 'NoEntryIDError'
        archive[entrycode] = d.copy()
    with open(path, 'w') as file:
        json.dump(archive, file)

def delete_archive(feedcode):
    """
    Input: string, the program-assigned feedcode.
    The function will attempt to delete the associated archive file.
    Output: True if the deletion is carried out, False if there is a
    FileNotFoundError.
    """
    path = BaseDirectory.save_data_path('togatareader/archive/') + (feedcode
                                                                  + '.json')
    try:
        remove(path)
        return True
    except FileNotFoundError:
        return False
    
if __name__ == '__main__':
    d = load_feeds()
    delete_feed(list(d.keys())[0])

